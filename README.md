# IFC4Add2Railway0p7p6-based extended schema for meshfree analysis
- Sep. 17, 2020
- IfcBridgeAddMeshfree Ver. 0.1 
## Developed by S.I. Park
- email: sang.i.park@colorado.edu
## To cite this Schema
- Park, S. I., Lee, S.-H., Almasi, A., & Song, J.-H. (2020). Extended IFC-based strong form meshfree collocation analysis of a bridge structure. Automation in Construction, 119, 103364. doi:10.1016/j.autcon.2020.103364.
## Web page
- [IFCBridgeAddMeshfree 0.1](https://openest.gitlab.io/ifcbridgeaddmeshfree_0p1)